package com.kozlov.Controller;

import com.kozlov.Model.MyTreeMap;
import com.kozlov.View.Printable;
import com.kozlov.View.ViewFirst;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class Controller {
    Map binaryTreeMap;
    public Map<String, Printable> methodsMenu;
    public Controller() {
        binaryTreeMap = new MyTreeMap<>();
    }
   public  void start(){
        ViewFirst viewFirst = new ViewFirst();
        Scanner scan = new Scanner(System.in);
        String keyMenu;
       binaryTreeMap.put(1,1);
       binaryTreeMap.put(3,15);
       binaryTreeMap.put(5,17);
       binaryTreeMap.put(2,3);
       binaryTreeMap.put(10,15);
       methodsMenu = new LinkedHashMap<>();
       methodsMenu.put("1", this::binaryTreeAdd);
       methodsMenu.put("2", this::binaryTreeGet);
       methodsMenu.put("3", this::binaryTreeDelete);
        do {
            viewFirst.outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = scan.nextLine().toUpperCase();
            try {
                viewFirst.methodsMenu.get(keyMenu);
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }
    void binaryTreeAdd () {
        binaryTreeMap.put(7,7);
    }
    void binaryTreeGet () {
        binaryTreeMap.get(1);
    }
    void binaryTreeDelete () {
        binaryTreeMap.remove(1);
    }
}
