package com.kozlov;


import com.kozlov.Controller.Controller;
import com.kozlov.View.ViewFirst;

public class Application {

  public static void main(String[] args) {
    Controller con = new Controller();
    con.start();
  }
}
