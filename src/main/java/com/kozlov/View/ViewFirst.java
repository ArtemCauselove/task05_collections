package com.kozlov.View;

import com.kozlov.Controller.Controller;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class ViewFirst {

    private Controller controller;
    private Map<String, String> menu;
    public Map<String, Printable> methodsMenu;
    private Scanner input;
    public ViewFirst() {
        controller = new Controller();
        input = new Scanner(System.in);
        menu = new LinkedHashMap<>();
        menu.put("1", "  1 - add number");
        menu.put("2", "  2 - get number");
        menu.put("3", "  3 - delete number");
        menu.put("Q", "  Q - exit");
    }
    public void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }
}
