package com.kozlov.View;

@FunctionalInterface
public interface Printable {

  void print();
}
